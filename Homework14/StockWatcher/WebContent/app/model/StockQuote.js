Ext.define('SW.model.StockQuote', {
    extend: 'Ext.data.Model',

    idProperty: 'symbol',

    fields: [
        { name: 'symbol',    type: 'string' },
        { name: 'name',      type: 'string' },
        { name: 'lastTrade', type: 'float' },
        { name: 'change',    type: 'float' }
    ]
});
