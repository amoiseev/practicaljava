Ext.define('SW.controller.Portfolio', {
    extend: 'Ext.app.Controller',

    stores: ['StockQuotes'],

    refs: [{
        ref: 'stockQuoteGrid',
        selector: 'stockquotelist'
    }],

    init: function() {
        this.control({
            'stockquotelist button[action=addsymbol]': {
                click: this.addSymbol
            }
        });
    },

    addSymbol: function(button) {
        var store = button.up('[store]').getStore(),
            combo = button.previousSibling('sscombobox'),
            symbol = combo.getValue() || combo.displayTplData[0].symbol;

        if (symbol) {
            Ext.Ajax.request({
                url: 'api/quotes/' + symbol,
                success: function(response) {
                    store.add(Ext.JSON.decode(response.responseText));
                }
            });
        }
    }
});
