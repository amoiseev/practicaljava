Ext.define('SW.view.stockquote.SymbolSuggestComboBox', {
	extend: 'Ext.form.field.ComboBox',
	xtype: 'sscombobox',
	
	hideTrigger: true,
	minChars: 2,
    fields: ['symbol', 'name'],
    valueField: 'symbol',
    displayField: 'name',
	store: Ext.create('SW.store.SymbolSuggests')
});
