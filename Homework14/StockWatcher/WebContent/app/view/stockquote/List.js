Ext.define('SW.view.stockquote.List', {
    extend: 'Ext.grid.Panel',
    xtype : 'stockquotelist',
    
    requires: [
        'SW.view.stockquote.SymbolSuggestComboBox'
	],

    store: 'StockQuotes',

    columns: [{
        dataIndex: 'symbol',
        text     : 'Symbol',
        flex     : 1
    }, {
        dataIndex: 'name',
        text     : 'Name',
        flex     : 3
    }, {
        dataIndex: 'lastTrade',
        text     : 'Last Trade',
        renderer : Ext.util.Format.usMoney,
        flex     : 1
    }, {
        xtype    : 'numbercolumn',
        dataIndex: 'change',
        text     : 'Change',
        format   : '0.00',
        flex     : 1,
        renderer : function (value, metaData) {
            value < 0
                ? metaData.style = 'color:red'
                : metaData.style = 'color:green';
            return value;
        }
    }],

    tbar: [{
		xtype: 'sscombobox',
		id: 'sscombobox',
		width: 350,
		emptyText: 'Enter stock symbol',
	},
    {
        xtype: 'button',
        text: 'Add to portfolio',
        action: 'addsymbol'
    }]
});
