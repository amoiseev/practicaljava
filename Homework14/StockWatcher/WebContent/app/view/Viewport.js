Ext.define('SW.view.Viewport', {
    extend: 'Ext.container.Viewport',

    requires: [
        'SW.view.stockquote.List'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'stockquotelist',
        title: 'Portfolio'
    }]
});
