Ext.define('SW.store.SymbolSuggests', {
	extend: 'Ext.data.Store',
	
	fields: ['symbol', 'name'],
	proxy: {
		type: 'jsonp',
		url: 'http://autoc.finance.yahoo.com/autoc?callback=YAHOO.Finance.SymbolSuggest.ssCallback'
	} 
});
