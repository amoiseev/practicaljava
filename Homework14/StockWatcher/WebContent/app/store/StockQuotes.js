Ext.define('SW.store.StockQuotes', {
    extend: 'Ext.data.Store',
    model: 'SW.model.StockQuote'
});
