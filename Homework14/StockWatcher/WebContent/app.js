// Define namespace for symbol suggest callback
// Callback name is mandatory by Yahoo Finance
Ext.ns("YAHOO.Finance.SymbolSuggest");

Ext.Loader.setConfig({
    enabled: true,         // Allow dynamic loading of JavaScript files
    disableCaching: false  // Disable random parameter in the URL's path
});

Ext.application({
    name              : 'SW', // Stock Watcher
    autoCreateViewport: true,
    controllers       : ['Portfolio']
});


YAHOO.Finance.SymbolSuggest.ssCallback = function (response) {
	var combo = Ext.getCmp('sscombobox'),
		store = combo.getStore(),
		data;

	// Bug workaround http://www.sencha.com/forum/showthread.php?153490-Combo-Box-Store-Loading 
	if (Ext.typeOf(combo.getPicker().loadMask) !== "boolean") {
        combo.getPicker().loadMask.hide();
    }
	
	// Prepare data before adding to combobox
	data = Ext.Array.map(response.ResultSet.Result, function (stock) {
		stock.name = stock.symbol + ' - ' + stock.name;
		return stock;
	});

	store.loadData(data);
};
