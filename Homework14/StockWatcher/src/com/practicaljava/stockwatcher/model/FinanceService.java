package com.practicaljava.stockwatcher.model;


public interface FinanceService {
    /**
     * Fetches quotes data for specified stock symbol.
     */
    StockQuote fetchQuote(String symbol);
}
