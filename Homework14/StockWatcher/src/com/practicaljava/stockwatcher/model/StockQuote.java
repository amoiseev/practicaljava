package com.practicaljava.stockwatcher.model;

public class StockQuote {
    public String symbol;
    public String name;
    public double lastTrade;
    public double change;
}
