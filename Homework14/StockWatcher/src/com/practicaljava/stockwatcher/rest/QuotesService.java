package com.practicaljava.stockwatcher.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;
import com.practicaljava.stockwatcher.model.YahooFinanceProxy;

@Path("/quotes")
public class QuotesService {

    private final FinanceService financeService;

    public QuotesService() {
        financeService = new YahooFinanceProxy();
    }

    @GET
    @Path("{symbol}")
    @Produces("application/json")
    public String getQuotes(@PathParam("symbol") String symbol) {
        StockQuote quote = financeService.fetchQuote(symbol);
        return new Gson().toJson(quote);
    }

}
