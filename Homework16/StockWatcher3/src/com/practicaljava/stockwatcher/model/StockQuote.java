package com.practicaljava.stockwatcher.model;

public class StockQuote {
    public String symbol;
    public String name;
    public String marketCap;
    public double price;
    public double change;
}
