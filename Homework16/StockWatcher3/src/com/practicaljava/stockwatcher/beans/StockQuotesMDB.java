package com.practicaljava.stockwatcher.beans;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;

@MessageDriven
public class StockQuotesMDB implements MessageListener {

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "Quotes")
    private Queue quotesQueue;

    public StockQuotesMDB() {
    }

    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            System.out.println("MESSAGE: " + textMessage.getText());
        } catch (JMSException e) {
            System.err.println(e.getMessage());
        }
    }

}
