package com.practicaljava.stockwatcher.beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;

import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;

import au.com.bytecode.opencsv.CSVParser;

@Stateless
public class YahooFinanceBean implements FinanceService {
    private static final String QUOTE_URL = "http://finance.yahoo.com/d/quotes.csv?s=%s&f=snl1c1j1";

    /**
     * Fetches quotes data for specified stock symbol.
     */
    public StockQuote fetchQuote(String symbol) {
        URL url = null;
        URLConnection urlConnection = null;
        InputStreamReader streamReader = null;
        BufferedReader bufferedReader = null;
        String csvString = null;

        try {
            url = new URL(String.format(QUOTE_URL, symbol));
            urlConnection = url.openConnection();
            streamReader = new InputStreamReader(urlConnection.getInputStream());
            bufferedReader = new BufferedReader(streamReader);
            csvString = bufferedReader.readLine();
        } catch (IOException e) {
            System.err.println("Error: request to external stock quotes service failed");
        } finally {
            try {
                streamReader.close();
                bufferedReader.close();
            } catch (Exception e) {
                System.err.println("Error: can't close external stock quotes service streams" + e.getMessage());
            }
        }

        // If stock quotes are successfully fetched deserialize data
        // to StockQuote object and return it to the invoking code.
        if (csvString != null) {
            StockQuote stockQuote = null;
            try {
                CSVParser csvParser = new CSVParser();
                String[] parts = csvParser.parseLine(csvString);
                stockQuote = new StockQuote();

                if (!"N/A".equalsIgnoreCase(parts[4])) {
                    stockQuote.symbol = StringUtils.strip(parts[0], "\"");
                    stockQuote.name = StringUtils.strip(parts[1], "\"");
                    stockQuote.price = Double.parseDouble(parts[2]);
                    stockQuote.change = Double.parseDouble(parts[3]);
                    stockQuote.marketCap = parts[4];
                }
            } catch (Exception e) {
                System.err.println(
                        "Error: can't deserialize external stock quotes service " +
                        "response into StockQuote object: " + csvString);
            }
            return stockQuote;
        }
        return null;
    }
}
