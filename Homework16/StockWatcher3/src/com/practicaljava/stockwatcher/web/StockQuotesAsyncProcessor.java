package com.practicaljava.stockwatcher.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.AsyncContext;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.beans.YahooFinanceBean;
import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;

public class StockQuotesAsyncProcessor implements Runnable {
    private final AsyncContext asyncContext;
    private final FinanceService financeService;

    public StockQuotesAsyncProcessor(AsyncContext asyncContext) {
        this.asyncContext = asyncContext;
        this.financeService = new YahooFinanceBean();
    }

    @Override
    public void run() {
        String symbol = (String)asyncContext.getRequest().getAttribute("symbol");
        StockQuote quote = financeService.fetchQuote(symbol);

        // Serialize StockQuote object to JSON.
        Gson gson = new Gson();
        String jsonQuote = gson.toJson(quote);

        // Write stock quote data to the response.
        PrintWriter out = null;
        try {
            out = asyncContext.getResponse().getWriter();
            out.write(jsonQuote);
        } catch (IOException e) {
            System.err.println("Error: can't get output stream for asynchronous request handling");
        } finally {
            out.close();
            asyncContext.complete();
        }
    }
}
