package com.practicaljava.stockwatcher.web;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.annotation.WebServlet;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;

@WebServlet(urlPatterns = "/quotesws")
public class StockQuotesWebSocketServlet extends WebSocketServlet {

    @EJB
    private FinanceService financeService;

    @Resource
    private ConnectionFactory connectionFactory;

    @Resource(name = "Quotes")
    private Queue quotesQueue;

    @Override
    protected StreamInbound createWebSocketInbound(String subProtocol) {

        return new MessageInbound() {
            @Override
            protected void onTextMessage(CharBuffer message) throws IOException {
                Gson gson = new Gson();

                // Parse incoming message to get stock symbols for update.
                String[] symbols = StringUtils.split(message.toString(), ',');

                enqueueSymbols(symbols);

                // TODO: replace synchronous requests dispatching with asynchronous one
                for (String symbol: symbols) {
                    StockQuote stockQuote = financeService.fetchQuote(symbol);

                    CharBuffer response = CharBuffer.wrap(gson.toJson(stockQuote));
                    this.getWsOutbound().writeTextMessage(response);
                }
            }

            @Override
            protected void onBinaryMessage(ByteBuffer byteBuffer) throws IOException {
            }
        };
    }

    /**
     * Send symbols to the queue to dispatch them asynchronously.
     */
    private void enqueueSymbols(String[] symbols) {
        Connection connection = null;
        Session session = null;

        try {
            connection = connectionFactory.createConnection();
            connection.start();

            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            MessageProducer producer = session.createProducer(quotesQueue);

            for (String s: symbols) {
                TextMessage message = session.createTextMessage(s);
                producer.send(message);
            }
        } catch (JMSException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    session.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

}
