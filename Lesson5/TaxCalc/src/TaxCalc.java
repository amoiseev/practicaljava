public class TaxCalc {

    public static void main(String[] args) {
        // Check number of provided arguments
        if (args.length != 3) {
            System.out.println(
                "The program expects 3 arguments: gross " +
                "income, number of dependents, state.\n" +
                "Sample usage: java TaxCalc 50000 2 NJ");
            return;
        }

        // Parse command line arguments
        double grossIncome;
        int dependents;
        String state;

        try {
            grossIncome = Double.parseDouble(args[0]);
            dependents = Integer.parseInt(args[1]);
            state = args[2];
        } catch (NumberFormatException e) {
            System.out.println(
                "Format of provided arguments is wrong.\n" +
                "Sample usage: java TaxCalc 50000 2 NJ");
            return;
        }

        // Calculate tax and output result
        Tax tax = new Tax(grossIncome, dependents, state);
        System.out.printf("The tax is: %s\n", tax.calcTax());
    }

}
