public class Tax {
    public final int TAX_RATE_CRITERIA = 30000;
    public final double SMALL_TAX_RATE = 0.06;
    public final double BIG_TAX_RATE = 0.08;

    public double grossIncome;
    public int dependents;
    public String state;

    public Tax(double grossIncome, int dependents, String state) {
        this.grossIncome = grossIncome;
        this.dependents = dependents;
        this.state = state;
    }

    public double calcTax() {
        return grossIncome > TAX_RATE_CRITERIA
                ? grossIncome * BIG_TAX_RATE
                : grossIncome * SMALL_TAX_RATE;
    }
}
