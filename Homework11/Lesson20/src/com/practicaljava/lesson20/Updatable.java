package com.practicaljava.lesson20;

public interface Updatable<T> {
    void setData(T data);
}
