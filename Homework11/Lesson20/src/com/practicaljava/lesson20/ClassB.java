package com.practicaljava.lesson20;

public class ClassB extends Thread {
    private final Updatable<String> caller;

    public ClassB(Updatable<String> caller) {
        this.caller = caller;
    }

    @Override
    public void run() {
        System.out.println("ClassB: updating caller's data");

        caller.setData("Economy is recovering");

        synchronized (caller) {
            System.out.println("ClassB: notifying caller");
            caller.notify();
        }
    }
}
