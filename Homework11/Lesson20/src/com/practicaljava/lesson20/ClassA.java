package com.practicaljava.lesson20;

public class ClassA implements Updatable<String> {
    public String marketNews = null;

    public void fetchData() {
        ClassB b = new ClassB(this);
        b.start();

        synchronized (this) {
            try {
                System.out.println("ClassA: suspending the main thread");
                wait();
            } catch (InterruptedException e) {
                System.out.println("ClassA: the main thread is interrupted");
            }
        }
    }

    public void setData(String news) {
        marketNews = news;
        System.out.printf("ClassA: the data is updated - \"%s\"\n", marketNews);
    }
}
