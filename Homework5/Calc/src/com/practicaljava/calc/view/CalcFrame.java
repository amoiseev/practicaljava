package com.practicaljava.calc.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.practicaljava.calc.controller.CalcController;

@SuppressWarnings("serial")
public class CalcFrame extends JFrame {
    private final int NUMBER_OF_DIGITS = 10;

    private JPanel contentPanel;     // Keeps all the UI components of the calculator
    private JTextField displayField; // Displays user's input and calculation results
    private JButton[] digitButtons;  // Keeps buttons for each digit
    private JButton plusButton;
    private JButton multButton;
    private JButton minusButton;
    private JButton periodButton;
    private JButton divideButton;
    private JButton evaluateButton;

    private CalcController controller;

    public CalcFrame() {
        // Initialize properties of the frame
        this.setResizable(false);
        this.setSize(300, 350);
        this.setTitle("Calculator");

        // Initialize controller
        controller = new CalcController(this);

        initComponents();
        bindListeners();
    }

    private void initComponents() {
        // Initialize main layout of the calculator
        GridBagLayout gridBag = new GridBagLayout();
        contentPanel = new JPanel();
        contentPanel.setLayout(gridBag);
        this.add(contentPanel);


        // Add display field on the content panel
        GridBagConstraints displayFieldConstr = new GridBagConstraints(
            0,   // gridx
            0,   // gridy
            6,   // gridwidth
            1,   // gridheight
            1.0, // weightx
            1.0, // weighty
            GridBagConstraints.NORTH, // anchor
            GridBagConstraints.BOTH,  // fill
            new Insets(5, 5, 5, 5),   // insets
            0,  // ipadx
            0); // ipady

        displayField = new JTextField();
        displayField.setEditable(false);
        displayField.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(displayField, displayFieldConstr);


        // Large font for digits and operation signs
        Font largeFont = new Font(null, 0, 32);


        // Initialize digit buttons
        GridBagConstraints digitButtonsConstr = new GridBagConstraints(
            0,   // gridx
            0,   // gridy
            1,   // gridwidth
            1,   // gridheight
            1.0, // weightx
            1.0, // weighty
            GridBagConstraints.CENTER, // anchor
            GridBagConstraints.BOTH,   // fill
            new Insets(0, 0, 0, 0),    // insets
            0,  // ipadx
            0); // ipady

        digitButtons = new JButton[NUMBER_OF_DIGITS];
        for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
            digitButtons[i] = new JButton(Integer.toString(i));
            digitButtons[i].setFont(largeFont);
            digitButtons[i].addActionListener(controller);
        }

        // First row, digits 7 - 9
        digitButtonsConstr.gridx = 0;
        digitButtonsConstr.gridy = 1;
        digitButtonsConstr.insets = new Insets(2, 5, 2, 2);
        contentPanel.add(digitButtons[7], digitButtonsConstr);

        digitButtonsConstr.gridx = 1;
        digitButtonsConstr.gridy = 1;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[8], digitButtonsConstr);

        digitButtonsConstr.gridx = 2;
        digitButtonsConstr.gridy = 1;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[9], digitButtonsConstr);

        // Second row, digits 4 - 6
        digitButtonsConstr.gridx = 0;
        digitButtonsConstr.gridy = 2;
        digitButtonsConstr.insets = new Insets(2, 5, 2, 2);
        contentPanel.add(digitButtons[4], digitButtonsConstr);

        digitButtonsConstr.gridx = 1;
        digitButtonsConstr.gridy = 2;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[5], digitButtonsConstr);

        digitButtonsConstr.gridx = 2;
        digitButtonsConstr.gridy = 2;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[6], digitButtonsConstr);

        // Third row, digits 1 - 3
        digitButtonsConstr.gridx = 0;
        digitButtonsConstr.gridy = 3;
        digitButtonsConstr.insets = new Insets(2, 5, 2, 2);
        contentPanel.add(digitButtons[1], digitButtonsConstr);

        digitButtonsConstr.gridx = 1;
        digitButtonsConstr.gridy = 3;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[2], digitButtonsConstr);

        digitButtonsConstr.gridx = 2;
        digitButtonsConstr.gridy = 3;
        digitButtonsConstr.insets = new Insets(2, 2, 2, 2);
        contentPanel.add(digitButtons[3], digitButtonsConstr);

        // Fourth row, digit 0
        digitButtonsConstr.gridx = 0;
        digitButtonsConstr.gridy = 4;
        digitButtonsConstr.insets = new Insets(2, 5, 5, 2);
        contentPanel.add(digitButtons[0], digitButtonsConstr);


        // Initialize operation buttons
        GridBagConstraints operationButtonsConstr = new GridBagConstraints(
            0,   // gridx
            0,   // gridy
            1,   // gridwidth
            1,   // gridheight
            1.0, // weightx
            1.0, // weighty
            GridBagConstraints.CENTER, // anchor
            GridBagConstraints.BOTH,   // fill
            new Insets(0, 0, 0, 0),    // insets
            0,  // ipadx
            0); // ipady

        operationButtonsConstr.gridx = 3;
        operationButtonsConstr.gridy = 1;
        operationButtonsConstr.insets = new Insets(2, 2, 2, 5);
        divideButton = new JButton("/");
        divideButton.setFont(largeFont);
        contentPanel.add(divideButton, operationButtonsConstr);

        operationButtonsConstr.gridx = 3;
        operationButtonsConstr.gridy = 2;
        operationButtonsConstr.insets = new Insets(2, 2, 2, 5);
        multButton = new JButton("*");
        multButton.setFont(largeFont);
        contentPanel.add(multButton, operationButtonsConstr);

        operationButtonsConstr.gridx = 3;
        operationButtonsConstr.gridy = 3;
        operationButtonsConstr.insets = new Insets(2, 2, 2, 5);
        minusButton = new JButton("-");
        minusButton.setFont(largeFont);
        contentPanel.add(minusButton, operationButtonsConstr);

        operationButtonsConstr.gridx = 3;
        operationButtonsConstr.gridy = 4;
        operationButtonsConstr.insets = new Insets(2, 2, 5, 5);
        plusButton = new JButton("+");
        plusButton.setFont(largeFont);
        contentPanel.add(plusButton, operationButtonsConstr);

        operationButtonsConstr.gridx = 2;
        operationButtonsConstr.gridy = 4;
        operationButtonsConstr.insets = new Insets(2, 2, 5, 2);
        evaluateButton = new JButton("=");
        evaluateButton.setFont(largeFont);
        contentPanel.add(evaluateButton, operationButtonsConstr);

        operationButtonsConstr.gridx = 1;
        operationButtonsConstr.gridy = 4;
        operationButtonsConstr.insets = new Insets(2, 2, 5, 2);
        periodButton = new JButton(".");
        periodButton.setFont(largeFont);
        contentPanel.add(periodButton, operationButtonsConstr);
    }

    public String getText() {
        return displayField.getText();
    }

    public void setText(String value) {
        displayField.setText(value);
    }

    private void bindListeners() {
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

}

