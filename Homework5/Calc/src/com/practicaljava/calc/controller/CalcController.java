package com.practicaljava.calc.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.practicaljava.calc.view.CalcFrame;

public class CalcController implements ActionListener {
    private final CalcFrame view;
    private Double lhsOperand;
    private Double rhsOperand;

    public CalcController(CalcFrame view) {
        this.view = view;
    }

    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        String text = button.getText();

        switch (text) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                String currentValue = view.getText();
                view.setText(currentValue + text);
                break;
            case "+":
                add();
                break;
            default:
                break;
        }
    }

    private void add() {
        if (lhsOperand == null) {
            lhsOperand = Double.parseDouble(view.getText());
        } else if (rhsOperand == null) {
            rhsOperand = Double.parseDouble(view.getText());
            view.setText(Double.toString(lhsOperand + rhsOperand));
        }
    }

}
