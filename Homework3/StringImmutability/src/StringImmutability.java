public class StringImmutability {

    public static void main(String[] args) {
        String gfriend = "Masha";
        System.out.println(gfriend == "Masha");

        String gfriend1 = new String("Natasha");
        String gfriend2 = new String("Natasha");
        String gfriend3 = "Natasha";

        System.out.println(gfriend1 == gfriend2);
        System.out.println(gfriend1 == gfriend3);
        System.out.println(gfriend1.equals(gfriend2));
        System.out.println(gfriend1.equals(gfriend3));
    }

}
