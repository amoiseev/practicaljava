import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.practicaljava.tictactoe.CellState;
import com.practicaljava.tictactoe.GameState;
import com.practicaljava.tictactoe.Grid;
import com.practicaljava.tictactoe.Player;
import com.practicaljava.tictactoe.Position;

@SuppressWarnings("serial")
public class TicTacToeApplet extends JApplet implements MouseListener
{
    private static final String PLAYERX = "Player X";
    private static final String PLAYERO = "Player O";

    private String playerName = PLAYERX;

    private JButton[] buttons;
    private JButton resetButton;
    private JLabel messageLabel;
    private Panel mainPanel;
    private Panel buttonsPanel;
    private Panel controlPanel;
    private Color initialColor;

    private Grid grid;

    public void init() {
        setSize(300, 400);
        initComponents();

        grid = new Grid();
        setMessage(PLAYERX + " your turn...");
    }

    private void initComponents() {
        Font buttonFont = new Font("Verdana", Font.PLAIN, 50);

        mainPanel = new Panel();
        mainPanel.setLayout(new BorderLayout());

        buttonsPanel = new Panel();
        buttonsPanel.setLayout(new GridLayout(4, 3));

        buttons = new JButton[9];
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].addMouseListener(this);
            buttons[i].setFont(buttonFont);
            buttons[i].setOpaque(true);

            buttonsPanel.add(buttons[i]);
        }

        controlPanel = new Panel();
        controlPanel.setLayout(new GridLayout(2, 1));

        messageLabel = new JLabel(playerName, SwingConstants.CENTER);
        controlPanel.add(messageLabel);

        resetButton = new JButton();
        resetButton.setText("Reset");
        resetButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        controlPanel.add(resetButton);
        initialColor = resetButton.getBackground();

        mainPanel.add(BorderLayout.CENTER, buttonsPanel);
        mainPanel.add(BorderLayout.NORTH, controlPanel);

        add(mainPanel);
    }

    private void setMessage(String message){
        messageLabel.setText(message);
    }

    private void reset(){
        grid = new Grid();
        for (JButton button: buttons) {
            button.setText("");
            button.setBackground(initialColor);
        }
        buttonsPanel.setEnabled(true);
        playerName = PLAYERX;
        setMessage(PLAYERX + " your turn...");
    }

   private boolean checkForWinner() {
        GameState gameState = grid.getGameState();

        switch (gameState) {
            case XWin:
                setMessage(PLAYERX + " won!");
                highlightLine(grid.findThreeInARow(CellState.X));
                return true;
            case OWin:
                setMessage(PLAYERO + " won!");
                highlightLine(grid.findThreeInARow(CellState.O));
                return true;
            case Draw:
                setMessage("Draw");
                buttonsPanel.setEnabled(false);
                return true;
            case Indeterminate:
                return false;
            default:
                return false;
        }

    }

   private void highlightLine(Position[] completeLine) {
       for (Position p: completeLine) {
           buttons[p.toSequential(Grid.SIZE)].setBackground(Color.GREEN);
       }

       buttonsPanel.setEnabled(false);
   }

    public void mouseClicked(MouseEvent e) {
        JButton clickedButton = (JButton)e.getComponent();
        int index = Arrays.asList(buttons).indexOf(clickedButton);
        Position position = Position.fromSequential(index, Grid.SIZE);

        if (clickedButton.getText() == "") {
            if (playerName == PLAYERX) {
                grid = new Grid(grid, position, Player.X);
                clickedButton.setText("X");

                playerName = PLAYERO;
                setMessage(PLAYERO + " your turn...");

                if (checkForWinner()) {
                    return;
                }

                grid = grid.findBestMove();
                Position lastPosition = grid.getLastPosition();
                buttons[lastPosition.toSequential(Grid.SIZE)].setText("O");

                playerName = PLAYERX;
                setMessage(PLAYERX + " your turn...");

                checkForWinner();
            }
        }
    }

    // Not implemented
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
}
