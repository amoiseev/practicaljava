package com.practicaljava.tictactoe;

public class Position {
    public final int x;
    public final int y;
    public CellState state;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        this.state = CellState.Blank;
    }

    public Position(int x, int y, CellState state) {
        this(x, y);
        this.state = state;
    }

    /**
     * Convert two dimensional position to sequential index
     * correctly handling the number of rows.
     * @param rows
     * @return
     */
    public int toSequential(int rows) {
        return y * rows + x;
    }

    /**
     * Convert sequential index to two dimensional position
     * correctly handling the number of rows.
     * @param index Sequential index to convert
     * @param rows Number of rows in the grid
     * @return Two dimensional position
     */
    public static Position fromSequential(int index, int rows) {
        return new Position(index % rows,
            (int)Math.ceil(index / rows));
    }
}
