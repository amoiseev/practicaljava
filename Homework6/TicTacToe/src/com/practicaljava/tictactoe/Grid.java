package com.practicaljava.tictactoe;

import java.util.ArrayList;

public class Grid {
    public static final int SIZE = 3;

    private CellState[][] grid;
    private Position lastPosition;

    public Grid() {
        grid = new CellState[SIZE][SIZE];

        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                grid[x][y] = CellState.Blank;
            }
        }
    }

    public Grid(Grid source, Position position, Player player) {
        grid = new CellState[SIZE][SIZE];

        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                grid[x][y] = source.grid[x][y];
            }
        }

        lastPosition = position;
        grid[position.x][position.y] = player == Player.X
            ? CellState.X
            : CellState.O;
    }

    public Position getLastPosition() {
        return lastPosition;
    }

    public Grid findBestMove() {
        ArrayList<Position> positions = getFreePositions();
        Grid bestMove = null;
        int best = Integer.MIN_VALUE;

        for (Position pos: positions) {
            Grid move = new Grid(this, pos, Player.O);
            int current = move.min();

            if (current > best) {
                bestMove = move;
                best = current;
                lastPosition = pos;
            }
        }
        return bestMove;
    }

    public int max() {
        GameState gameState = getGameState();

        switch (gameState) {
            case XWin: return -1;
            case OWin: return 1;
            case Draw: return 0;
        }

        ArrayList<Position> freePositions = getFreePositions();
        int best = Integer.MIN_VALUE;

        for (Position pos: freePositions) {
            Grid g = new Grid(this, pos, Player.O);
            int move = g.min();

            if (move > best) {
                best = move;
            }
        }

        return best;
    }

    public int min() {
        GameState gameState = getGameState();

        switch (gameState) {
            case XWin: return -1;
            case OWin: return 1;
            case Draw: return 0;
        }

        ArrayList<Position> freePositions = getFreePositions();
        int best = Integer.MAX_VALUE;

        for (Position pos: freePositions) {
            Grid g = new Grid(this, pos, Player.X);
            int move = g.max();

            if (move < best) {
                best = move;
            }
        }
        return best;
    }

    public ArrayList<Position> getFreePositions() {
        ArrayList<Position> freePositions = new ArrayList<Position>();

        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                if (grid[x][y] == CellState.Blank) {
                    freePositions.add(new Position(x, y));
                }
            }
        }
        return freePositions;
    }

    public GameState getGameState() {
        if (findThreeInARow(CellState.X) != null) {
            return GameState.XWin;
        } else if (findThreeInARow(CellState.O) != null) {
            return GameState.OWin;
        } else if (getFreePositions().size() == 0) {
            return GameState.Draw;
        } else {
            return GameState.Indeterminate;
        }
    }

    public Position[] findThreeInARow(CellState cellState) {
        Position[] completeLine = new Position[SIZE];
        boolean found;

        // Check rows
        for (int y = 0; y < SIZE; y++) {
            found = true;

            for (int x = 0; x < SIZE; x++) {
                found &= grid[x][y] == cellState;
                completeLine[x] = new Position(x, y);
            }

            if (found) {
                return completeLine;
            }
        }

        // Check columns
        for (int x = 0; x < SIZE; x++) {
            found = true;

            for (int y = 0; y < SIZE; y++) {
                found &= grid[x][y] == cellState;
                completeLine[y] = new Position(x, y);
            }

            if (found) {
                return completeLine;
            }
        }

        // Check left diagonal
        found = true;
        for (int x = 0, y = 0; x < SIZE && y < SIZE; x++, y++) {
            found &= grid[x][y] == cellState;
            completeLine[x] = new Position(x, y);
        }
        if (found) {
            return completeLine;
        }

        // Check right diagonal
        found = true;
        for (int x = SIZE - 1, y = 0; x >= 0 && y < SIZE; x--, y++) {
            found &= grid[x][y] == cellState;
            completeLine[x] = new Position(x, y);
        }
        if (found) {
            return completeLine;
        }

        // If nothing is found return null
        return null;
    }

    public String toString() {
        String str = "";
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                str += grid[x][y] + " ";
            }
            str += "\n";
        }
        return str;
    }
}
