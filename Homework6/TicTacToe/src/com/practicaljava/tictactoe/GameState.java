package com.practicaljava.tictactoe;

public enum GameState {
    XWin,
    OWin,
    Draw,
    Indeterminate
}
