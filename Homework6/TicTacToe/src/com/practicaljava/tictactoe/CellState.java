package com.practicaljava.tictactoe;

public enum CellState {
    X,
    O,
    Blank
}
