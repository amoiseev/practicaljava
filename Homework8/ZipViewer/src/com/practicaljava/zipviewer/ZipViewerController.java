package com.practicaljava.zipviewer;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

public class ZipViewerController extends WindowAdapter implements ActionListener {
    private final ZipViewerFrame view;

    public ZipViewerController(ZipViewerFrame view) {
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem)e.getSource();

        if (item == view.openItem) {
            openFile();
        } else if (item == view.closeItem) {
            closeFile();
        }
    }

    private void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        // Continue reading only if user clicked "Open" button
        if (fileChooser.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            ZipReader reader = new ZipReader(file);

            ArrayList<Object[]> data =  reader.getData();
            view.setData(data);
            view.closeItem.setEnabled(true);
        }
    }

    private void closeFile() {
        view.setData(new ArrayList<Object[]>());
        view.closeItem.setEnabled(false);
    }

    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }
}
