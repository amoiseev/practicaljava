package com.practicaljava.zipviewer;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

@SuppressWarnings("serial")
public class SizeRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        long bytes = (Long)value;
        String readableSize = SizeConverter.getReadableSize(bytes);

        setText(readableSize);
        return this;
    }
}
