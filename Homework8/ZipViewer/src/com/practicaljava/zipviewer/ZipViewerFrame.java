package com.practicaljava.zipviewer;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class ZipViewerFrame extends JFrame {
    private final ZipViewerController controller;
    private JTable table;

    // Following menu items should be visible for the controller.
    JMenuItem openItem;
    JMenuItem closeItem;

    private static final String[] columnNames = {
        "Type",
        "Name",
        "Original Size",
        "Compressed Size",
        "Compress Ratio"
    };

    public ZipViewerFrame() {
        controller = new ZipViewerController(this);

        // Set frame's preferences
        setTitle("ZIP Viewer");
        setMinimumSize(new Dimension(700, 300));
        setLayout(new BorderLayout());

        // Adds handler for windowClosing event to correctly shutdown the application.
        addWindowListener(controller);
        initComponents();
    }

    /**
     * Initializes all UI components of the frame.
     */
    private void initComponents() {
        // Table displays list of entries inside ZIP file
        table = new JTable(new ZipFilesTableModel());
        table.setFillsViewportHeight(true);

        // Initialize each column's width with appropriate value.
        adjustColumns(table);

        // Scroll pane displays scroll bar if content doesn't fit one screen.
        JScrollPane scrollPane = new JScrollPane(table);

        // Initialize main menu
        openItem = new JMenuItem("Open...");
        openItem.addActionListener(controller);
        closeItem = new JMenuItem("Close");
        closeItem.addActionListener(controller);
        closeItem.setEnabled(false);

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(openItem);
        fileMenu.add(closeItem);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);

        // Add components to the frame
        add(menuBar, BorderLayout.PAGE_START);
        add(scrollPane, BorderLayout.CENTER);
    }

    /**
     * Sets appropriate width for each column of the JTable.
     */
    private void adjustColumns(JTable table) {
        List<String> columnNamesList = Arrays.asList(columnNames);

        for (String colName : columnNames) {
            int colIndex = columnNamesList.indexOf(colName);
            TableColumn col = table.getColumnModel().getColumn(colIndex);

            switch (colName) {
                case "Type":
                    col.setMinWidth(60);
                    col.setMaxWidth(60);
                    col.setCellRenderer(new TypeRenderer());
                    break;
                case "Name":
                    col.setPreferredWidth(600);
                    break;
                case "Original Size":
                    col.setMinWidth(90);
                    col.setPreferredWidth(90);
                    col.setCellRenderer(new SizeRenderer());
                    break;
                case "Compressed Size":
                    col.setMinWidth(120);
                    col.setPreferredWidth(120);
                    col.setCellRenderer(new SizeRenderer());
                    break;
                case "Compress Ratio":
                    col.setMinWidth(110);
                    col.setMaxWidth(110);
                    col.setCellRenderer(new CompressRatioRenderer());
                    break;
            }
        }
    }

    public void setData(ArrayList<Object[]> data) {
        table.setModel(new ZipFilesTableModel(data));
        adjustColumns(table);
    }

    class ZipFilesTableModel extends AbstractTableModel {
        private ArrayList<Object[]> data;

        ZipFilesTableModel() {
            this.data = new ArrayList<>();
        }

        ZipFilesTableModel(ArrayList<Object[]> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 4) {
                return new Object[] {
                    data.get(rowIndex)[2],
                    data.get(rowIndex)[3]
                };
            }
            return data.get(rowIndex)[columnIndex];
        }

        @Override
        @SuppressWarnings({ "unchecked", "rawtypes" })
        public Class getColumnClass(int columnIndex) {
            return getValueAt(0, columnIndex).getClass();
        }

        @Override
        public boolean isCellEditable(int rowIndex, int colIndex) {
            return false;
        }
    }
}
