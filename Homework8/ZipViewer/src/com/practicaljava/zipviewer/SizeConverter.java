package com.practicaljava.zipviewer;

public class SizeConverter {
    private SizeConverter() {
    }

    private static final long KBYTE = 1024;
    private static final long MBYTE = KBYTE * KBYTE;
    private static final long GBYTE = MBYTE * KBYTE;
    private static final long TBYTE = GBYTE * KBYTE;

    public static String getReadableSize(long bytes) {
        if (bytes < 0) {
            return "Unknown";
        }

        if (bytes < KBYTE) {
            return String.format("%d B", bytes);
        } else if (bytes < MBYTE) {
            return String.format("%d KB", bytes / KBYTE);
        } else if (bytes < GBYTE) {
            return String.format("%d MB", bytes / MBYTE);
        } else if (bytes < TBYTE) {
            return String.format("%d GB", bytes / GBYTE);
        } else {
            return String.format("%d TB", bytes / TBYTE);
        }

    }
}
