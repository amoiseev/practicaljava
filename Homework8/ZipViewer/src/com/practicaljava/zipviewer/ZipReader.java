package com.practicaljava.zipviewer;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipReader {
    private final File file;

    public ZipReader(File file) {
        this.file = file;
    }

    public ArrayList<Object[]> getData() {
        ArrayList<Object[]> data = new ArrayList<>();

        try (
            InputStream fileInputStream = new FileInputStream(file);
            InputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            ZipInputStream zipInputStream = new ZipInputStream(bufferedInputStream)
        ) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                Object[] properties = {
                    entry.isDirectory(),
                    entry.getName(),
                    entry.getSize(),
                    entry.getCompressedSize()
                };
                data.add(properties);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return data;
    }
}
