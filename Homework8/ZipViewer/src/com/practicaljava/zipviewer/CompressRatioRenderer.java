package com.practicaljava.zipviewer;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

@SuppressWarnings("serial")
public class CompressRatioRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        Object[] params = (Object[])value;
        long size = (Long)params[0];
        long compressedSize = (Long)params[1];

        if (size == -1 || compressedSize == -1 ) {
            setText("None");
        } else if (size == 0 || compressedSize == 0) {
            setText("0");
        } else {
            setText(String.format("%.2f", (double)size / (double)compressedSize));
        }

        return this;
    }
}
