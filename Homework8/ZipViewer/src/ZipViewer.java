import com.practicaljava.zipviewer.ZipViewerFrame;

import javax.swing.*;

public class ZipViewer {
    public static void main(String[] args) {

        //Schedule a job for the event-dispatching thread.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new ZipViewerFrame();
                frame.setVisible(true);
            }
        });
    }
}
