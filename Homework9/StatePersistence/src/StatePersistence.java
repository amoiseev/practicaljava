import com.practicaljava.statepersistence.*;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class StatePersistence {
    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new PreferencesFrame();
                frame.setVisible(true);
            }
        });
    }
}
