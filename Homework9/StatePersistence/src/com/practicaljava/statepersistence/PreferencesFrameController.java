package com.practicaljava.statepersistence;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class PreferencesFrameController extends WindowAdapter {
    private static final String PREFERENCES_FILE_NAME = "preferences.ser";
    private final PreferencesFrame view;

    public PreferencesFrameController(PreferencesFrame view) {
        this.view = view;
        restorePreferences();
    }

    private void restorePreferences() {
        UserPreferences preferences = readFromFile();
        if (preferences != null) {
            view.setBounds(preferences.bounds);
            view.setColor(preferences.color);
            view.setText(preferences.text);
        }
    }

    public void windowClosing(WindowEvent e) {
        UserPreferences preferences = new UserPreferences();
        preferences.bounds = view.getBounds();
        preferences.color = view.getColor();
        preferences.text = view.getText();
        saveToFile(preferences);

        System.exit(0);
    }

    private void saveToFile(UserPreferences preferences) {
        try (
            OutputStream fileStream = new FileOutputStream(PREFERENCES_FILE_NAME);
            OutputStream bufferedStream = new BufferedOutputStream(fileStream);
            ObjectOutputStream objectStream = new ObjectOutputStream(bufferedStream)
        ) {
            objectStream.writeObject(preferences);
        } catch (IOException e) {
            System.out.println("An error occurred while saving preferences into file.");
        }
    }

    private UserPreferences readFromFile() {
        UserPreferences preferences = new UserPreferences();
        try (
            InputStream fileStream = new FileInputStream(PREFERENCES_FILE_NAME);
            InputStream bufferedInputStream = new BufferedInputStream(fileStream);
            ObjectInputStream objectStream = new ObjectInputStream(bufferedInputStream)
        ) {
            preferences = (UserPreferences)objectStream.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("File with preferences is not found. Default settings will be used.");
        } catch (ClassNotFoundException | IOException e) {
            System.out.println("An error occurred while reading preferences from file. Default settings will be used");
        }
        return preferences;
    }
}
