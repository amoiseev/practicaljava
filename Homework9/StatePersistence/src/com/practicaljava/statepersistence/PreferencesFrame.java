package com.practicaljava.statepersistence;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.reflect.Array;
import java.util.Arrays;

public class PreferencesFrame extends JFrame {
    private final Color[] COLORS = { Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW };
    private JComboBox<Color> colors;
    private JTextField textField;

    public PreferencesFrame() {
        // Set frame's preferences
        setTitle("State Persistence");
        setLayout(new BorderLayout());
        setMinimumSize(new Dimension(200, 200));
        setMaximumSize(new Dimension(400, 400));

        // Initialize all frame's components
        initComponents();

        PreferencesFrameController controller = new PreferencesFrameController(this);
        addWindowListener(controller);
    }

    private void initComponents() {
        GridBagConstraints c;

        JPanel content = new JPanel(new GridBagLayout());
        add(content, BorderLayout.PAGE_START);

        JLabel colorLabel = new JLabel("Color");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        content.add(colorLabel, c);

        ColorRenderer colorRenderer = new ColorRenderer();
        colorRenderer.setPreferredSize(new Dimension(200, 30));
        colors = new JComboBox<>(COLORS);
        colors.setEditable(false);
        colors.setRenderer(colorRenderer);
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 3;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        content.add(colors, c);

        JLabel textLabel = new JLabel("Text");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        content.add(textLabel, c);

        textField = new JTextField();
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 3;
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = GridBagConstraints.HORIZONTAL;
        content.add(textField, c);
    }

    public Color getColor() {
        return (Color)colors.getSelectedItem();
    }

    public void setColor(Color color) {
        colors.setSelectedItem(color);
    }

    public String getText() {
        return textField.getText();
    }

    public void setText(String text) {
        textField.setText(text);
    }
}
