package com.practicaljava.statepersistence;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;

public class ColorRenderer extends JLabel implements ListCellRenderer {
    private static final HashMap<Color, String> COLOR_MAP;

    static {
        COLOR_MAP = new HashMap<>();
        COLOR_MAP.put(Color.BLACK, "Black");
        COLOR_MAP.put(Color.BLUE, "Blue");
        COLOR_MAP.put(Color.RED, "Red");
        COLOR_MAP.put(Color.GREEN, "Green");
        COLOR_MAP.put(Color.YELLOW, "Yellow");
    }

    public ColorRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        Color color = (Color)value;
        setBackground(color);
        if (COLOR_MAP.containsKey(color)) {
            setText(COLOR_MAP.get(color));
        }
        return this;
    }
}
