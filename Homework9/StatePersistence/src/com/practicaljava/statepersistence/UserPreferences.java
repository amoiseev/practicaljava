package com.practicaljava.statepersistence;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.Serializable;

public class UserPreferences implements Serializable {
    Rectangle bounds;
    String text;
    Color color;

    public UserPreferences() {
        bounds = new Rectangle(0, 0, 400, 300);
        color = Color.GREEN;
        text = "None";
    }
}
