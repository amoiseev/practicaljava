package com.practicaljava.bikestore;

@SuppressWarnings("serial")
public class TooManyBikesException extends Exception {
    public final String bike;
    public final int requestedQuantity;

    public TooManyBikesException(String bike, int requestedQuantity) {
        this.bike = bike;
        this.requestedQuantity = requestedQuantity;
    }
}
