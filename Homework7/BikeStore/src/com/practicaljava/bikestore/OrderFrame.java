package com.practicaljava.bikestore;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class OrderFrame extends JFrame implements ActionListener {
    private final int MAX_QUANTITY = 2;

    JComboBox<String> bikeList;
    JSpinner quantitySpinner;
    JButton processButton;
    JTextArea logText;

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                OrderFrame frame = new OrderFrame();
                frame.setVisible(true);
            }
        });

    }

    public OrderFrame() {
        // Set frame's preferences
        setSize(350, 300);
        setTitle("Bike Store");

        initComponents();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == processButton) {
            try {
                processOrder();
            } catch (TooManyBikesException ex) {
                logText.append("Exception TooManyBikesException:\n" +
                        ex.toString() + "\n\n");
            }
        }
    }

    private void processOrder() throws TooManyBikesException {
        int qunatity = (int)quantitySpinner.getValue();
        String bike = (String)bikeList.getSelectedItem();

        if (qunatity > MAX_QUANTITY) {
            throw new TooManyBikesException(bike, qunatity);
        } else {
            logText.append(String.format(
                "Order successfully processed. %s - %s\n\n",
                bike, qunatity));
        }
    }

    public void initComponents() {
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new GridBagLayout());
        add(contentPanel);

        GridBagConstraints c = new GridBagConstraints(
            0,   // gridx
            0,   // gridy
            1,   // gridwidth
            1,   // gridheight
            1.0, // weightx
            0,   // weighty
            GridBagConstraints.NORTH,      // anchor
            GridBagConstraints.HORIZONTAL, // fill
            new Insets(5, 5, 5, 5),        // insets
            0,  // ipadx
            0); // ipady

        // Add bikes combo box
        JLabel bikeLabel = new JLabel();
        bikeLabel.setText("Choose bike");
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 9, 5, 5);
        contentPanel.add(bikeLabel, c);

        String[] bikes = { "Bike 1", "Bike 2", "Bike 3" };

        bikeList = new JComboBox<String>(bikes);
        c.gridx = 0;
        c.gridy = 1;
        c.insets = new Insets(0, 5, 5, 5);
        contentPanel.add(bikeList, c);

        // Add quantity spinner
        JLabel quantityLabel = new JLabel();
        quantityLabel.setText("Quantity");
        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(5, 9, 5, 5);
        contentPanel.add(quantityLabel, c);

        SpinnerModel spinnerModel = new SpinnerNumberModel(
            0,  // value
            0,  // minimum
            10, // maximum
            1); // step

        quantitySpinner = new JSpinner(spinnerModel);
        c.gridx = 0;
        c.gridy = 3;
        c.insets = new Insets(0, 5, 5, 5);
        contentPanel.add(quantitySpinner, c);

        // Add process order button
        processButton = new JButton();
        processButton.setText("Process Order");
        processButton.addActionListener(this);
        c.gridx = 0;
        c.gridy = 4;
        c.insets = new Insets(0, 5, 5, 5);
        contentPanel.add(processButton, c);

        // Add exceptions log
        JLabel logLabel = new JLabel();
        logLabel.setText("Log");
        c.gridx = 0;
        c.gridy = 5;
        c.insets = new Insets(5, 9, 5, 5);
        contentPanel.add(logLabel, c);

        logText = new JTextArea();
        logText.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(logText);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 6;
        c.weighty = 1.0;
        c.insets = new Insets(0, 5, 5, 5);
        contentPanel.add(scrollPane, c);
    }
}
