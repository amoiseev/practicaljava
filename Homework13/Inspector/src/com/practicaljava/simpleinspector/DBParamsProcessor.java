package com.practicaljava.simpleinspector;

public class DBParamsProcessor {
    private static final String DEFAULT_TARGET_CLASS_NAME = "com.practicaljava.simpleinspector.MySQLWorker";

    public static void main(String[] args) {
        String targetClassName = getTargetClassName(args);

        try {
            Class<?> targetClass = Class.forName(targetClassName);

            if (targetClass.isAnnotationPresent(DBParams.class)) {
                DBParams dbParams = targetClass.getAnnotation(DBParams.class);

                System.out.printf(
                        "%s has following DB parameters:\n" +
                        "  DB Name (dbName):\t%s\n" +
                        "  User ID (uid):\t%s\n" +
                        "  Password (password):\t%s\n",
                        targetClassName, dbParams.dbName(), dbParams.uid(), dbParams.password());
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Class with specified name isn't found.");
        }
    }

    private static String getTargetClassName(String[] args) {
        if (args.length > 0) {
            return args[0];
        } else {
            System.out.printf(
                    "Target class name isn't provided. " +
                    "Default value \"%s\" will be used.\n\n",
                    DEFAULT_TARGET_CLASS_NAME);

            System.out.println("Usage: java SimpleInspector fullClassName");
            System.out.println("Example: java SimpleInspector com.practicaljava.simpleinspector.MySQLWorker\n");

            return DEFAULT_TARGET_CLASS_NAME;
        }
    }
}
