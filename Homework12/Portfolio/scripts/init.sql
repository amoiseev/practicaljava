CREATE TABLE Portfolio(
    id INT NOT NULL PRIMARY KEY,
    symbol VARCHAR(20) NOT NULL,
    quantity INT NOT NULL,
    price DECIMAL(6,2) NOT NULL
);

INSERT INTO Portfolio VALUES
    (1, 'IBM', 500, 105.50),
    (2, 'AMZN', 1000, 15.25),
    (3, 'AAPL', 2000, 275.50);
