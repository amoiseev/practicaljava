import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.practicaljava.portfolio.PortfolioFrame;

public class TestPortfolio {
    public static void main(String[] args) {

        // Schedule a job for the event-dispatching thread
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new PortfolioFrame();
                frame.setVisible(true);
            }
        });
    }
}
