package com.practicaljava.portfolio;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.practicaljava.portfolio.data.Order;

@SuppressWarnings("serial")
public class PortfolioTableModel extends AbstractTableModel {

    // Defines all column names that are used in the table.
    private static final String[] COLUMN_NAMES = {
        "Order ID",
        "Symbol",
        "Quantity",
        "Price"
    };

    // Keeps portfolio data.
    private List<Order> data;

    public PortfolioTableModel() {
        this.data = new ArrayList<>();
    }

    public PortfolioTableModel(List<Order> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return data.get(rowIndex).id;
            case 1: return data.get(rowIndex).symbol;
            case 2: return data.get(rowIndex).quantity;
            case 3: return data.get(rowIndex).price;
            default:
                return "";
        }
    }
}
