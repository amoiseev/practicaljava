package com.practicaljava.portfolio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.practicaljava.portfolio.data.Order;

@SuppressWarnings("serial")
public class PortfolioFrame extends JFrame {
    private static final ImageIcon SPINNER_ICON = new ImageIcon("resources/images/spinner.gif");

    // Controller associated with the frame and portfolio table.
    private final PortfolioController controller;

    // The table displays portfolio data.
    private JTable table;
    private JButton loadButton;
    private JButton clearButton;

    public PortfolioFrame() {
        controller = new PortfolioController(this);

        // Set frame's preferences
        setTitle("Portfolio");
        setMinimumSize(new Dimension(500, 200));
        setLayout(new BorderLayout());
        addWindowListener(controller);

        initComponents();
    }

    /**
     * Creates and initializes UI components on the frame.
     */
    private void initComponents() {
        table = new JTable();
        table.setModel(new PortfolioTableModel());
        table.setFillsViewportHeight(true);

        // Scroll pane displays scroll bar
        // if the table's content doesn't fit one screen.
        JScrollPane scrollPane = new JScrollPane(table);

        // Loads portfolio data from database to the table.
        loadButton = new JButton();
        loadButton.setName("LoadButton");
        loadButton.setText("Load Portfolio");
        loadButton.addActionListener(controller);
        loadButton.setFont(new Font(null, 0, 13));

        // Clears all the data in the table when the table isn't empty.
        clearButton = new JButton();
        clearButton.setName("ClearButton");
        clearButton.setText("Clear Table");
        clearButton.setEnabled(false);
        clearButton.addActionListener(controller);
        clearButton.setFont(new Font(null, 0, 13));

        // Keeps control buttons.
        JPanel panel = new JPanel();
        panel.add(loadButton);
        panel.add(clearButton);

        // Add components to the frame.
        add(scrollPane, BorderLayout.CENTER);
        add(panel, BorderLayout.SOUTH);
    }

    /**
     * Initializes portfolio table with new data.
     */
    public void setData(List<Order> data) {
        table.setModel(new PortfolioTableModel(data));
    }

    /**
     * Use to enable or disable Clear button.
     */
    public void setEnabledClearButton(boolean enabled) {
        clearButton.setEnabled(enabled);
    }

    /**
     * Use to enable or disable Load button.
     */
    public void setEnabledLoadButton(boolean enabled) {
        if (!enabled) {
            loadButton.setIcon(SPINNER_ICON);
        } else {
            loadButton.setIcon(null);
        }
        loadButton.setEnabled(enabled);
    }
}
