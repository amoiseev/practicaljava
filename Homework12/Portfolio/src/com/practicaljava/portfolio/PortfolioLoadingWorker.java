package com.practicaljava.portfolio;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingWorker;

import com.practicaljava.portfolio.data.Order;
import com.practicaljava.portfolio.data.Portfolio;

public class PortfolioLoadingWorker extends SwingWorker<List<Order>, String> {
    private final PortfolioFrame frame;

    public PortfolioLoadingWorker(PortfolioFrame frame) {
        this.frame = frame;
    }

    @Override
    protected List<Order> doInBackground() {
        try {
            // Suspend thread to imitate data loading.
            TimeUnit.MILLISECONDS.sleep(2000);
        } catch (InterruptedException e) {
            // Ignore exception
            e.printStackTrace();
        }
        return Portfolio.getAll();
    }

    @Override
    protected void done() {
        try {
            frame.setData(get());
            frame.setEnabledLoadButton(true);
            frame.setEnabledClearButton(true);
        } catch (Exception e) {
            // Ignore exception
            e.printStackTrace();
        }
    }
}
