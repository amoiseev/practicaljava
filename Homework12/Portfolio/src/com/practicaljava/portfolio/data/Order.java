package com.practicaljava.portfolio.data;

import java.math.BigDecimal;

/**
 * Represents single portfolio entry.
 */
public class Order {
    public final int id;
    public final int quantity;
    public final String symbol;
    public final BigDecimal price;

    public Order(int id, int quantity, String symbol, BigDecimal price) {
        this.id = id;
        this.price = price;
        this.symbol = symbol;
        this.quantity = quantity;
    }
}
