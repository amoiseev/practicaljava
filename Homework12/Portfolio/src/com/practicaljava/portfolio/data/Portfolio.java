package com.practicaljava.portfolio.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Provides static methods to access data in portfolio database.
 */
public final class Portfolio {
    private static final String CONNECTION_STRING = "jdbc:derby:data/PortfolioDB";

    /**
     * Prevent instantiating the class.
     */
    private Portfolio() {
    }

    public static ArrayList<Order> getAll() {
        ArrayList<Order> entryList = new ArrayList<>();

        Connection connection = null;
        Statement selectAllQuery = null;
        ResultSet entrySet = null;

        try {
            connection = DriverManager.getConnection(CONNECTION_STRING);
            selectAllQuery = connection.createStatement();
            entrySet = selectAllQuery.executeQuery("SELECT * FROM Portfolio");

            while (entrySet.next()) {
                Order entry = new Order(
                        entrySet.getInt("id"),
                        entrySet.getInt("quantity"),
                        entrySet.getString("symbol"),
                        entrySet.getBigDecimal("price"));

                entryList.add(entry);
            }
        } catch (SQLException e) {
            System.out.println(String.format("SQLException: %s; code: %s",
                    e.getMessage(),
                    e.getErrorCode()));
        } finally {
            try {
                entrySet.close();
                selectAllQuery.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return entryList;
    }
}
