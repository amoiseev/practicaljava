package com.practicaljava.portfolio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.practicaljava.portfolio.data.Order;

/**
 * Main application controller. Handles frame and portfolio table events.
 */
public class PortfolioController extends WindowAdapter implements ActionListener, TableModelListener {

    // Frame associated with the controller.
    private final PortfolioFrame frame;

    public PortfolioController(PortfolioFrame frame) {
        this.frame = frame;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();

        switch (button.getName()) {
            case "LoadButton":
                loadPortfolio();
                break;
            case "ClearButton":
                clearTable();
                break;
            default:
                break;
        }
    }

    private void loadPortfolio() {
        frame.setEnabledLoadButton(false);

        PortfolioLoadingWorker worker = new PortfolioLoadingWorker(frame);
        worker.execute();
    }

    private void clearTable() {
        frame.setData(new ArrayList<Order>());
        frame.setEnabledClearButton(false);
    }
}
