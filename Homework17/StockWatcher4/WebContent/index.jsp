<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Stock Watcher</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/lib/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
</head>

<body>
  <!-- Navbar -->
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">

        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="nav-collapse"></div>

      </div>
    </div>
  </div>

  <!-- Main content -->
  <div class="container">
    
    <div class="alert alert-success dn" id="alert-websockets-enabled">
      <button class="close" data-dismiss="alert">×</button>
      <strong>Good news!</strong> Your browser is cool and supports WebSockets!
      <strong>Live Update</strong> feature is enabled.
    </div>
    
    <div class="alert alert-error dn" id="alert-websockets-disabled">
      <button class="close" data-dismiss="alert">×</button>
      <strong>Bad news.</strong> Your browser does not support WebSockets.
      <strong>Live Update</strong> feature is disabled. To get the most recent
      stock quotes you need to refresh the page and request quotes data once again.
    </div>
  
    <header class="subheader">
      <h1>Stock Watcher</h1>
      <p class="lead">Manage your personal finance with our rock-solid the world-leading service</p>
    </header>

    <section id="portfolio">
      <h2>Portfolio</h2>
      <div class="row">
        <div class="span4">
          <p>Start typing symbol or company name you are interested in, and our service will offer you available options.
             Choose one and press <code>Add</code> button to add selected stock to your portfolio.</p>
          <div class="input-append">
            <input class="span3" type="text" data-bind="value: symbol" data-provide="typeahead" id="symbol"/><button class="btn btn-primary" type="button" data-bind="click: addStock">Add</button>
          </div>
          <br/>

          <div class="alert dn" id="alert-duplicate-symbol">
            <strong>Warning!</strong> Symbol <strong data-bind="text: symbol"></strong> is already in your portfolio.
          </div>
          <div class="alert dn" id="alert-invalid-symbol">
            <strong>Warning!</strong> There is no such symbol as <strong data-bind="text: invalidSymbol"></strong>.
          </div>
        </div>

        <!-- Right column with stock quotes table -->
        <div class="span8">
          <table class="table table-striped table-bordered">
            <colgroup>
              <col width="10%"/>
              <col width="50%"/>
              <col width="20%"/>
              <col width="20%"/>
            </colgroup>
            <thead>
              <tr>
                <th>Symbol</th>
                <th>Company Name</th>
                <th>Price</th>
                <th>Market Cap</th>
              </tr>
            </thead>
            <tbody data-bind="foreach: stocks">
              <tr data-bind="css: { temporary: isTemporary }">
                <td data-bind="text: symbol"></td>
                <td>
                  <span data-bind="css: { progressIcon: isTemporary }"></span>
                  <span data-bind="ifnot: isTemporary"><span data-bind="text: name"></span></span>
                </td>
                <td data-bind="css: { green: change() > 0, red: change() < 0 }">
                  <span data-bind="text: price"></span>
                  <small data-bind="text: change"></small>
                </td>
                <td data-bind="text: marketCap"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <!-- Scripts -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="assets/lib/jquery/jquery-1.8.0.min.js"></script>
  <script src="assets/lib/knockout/knockout-2.1.0.min.js"></script>
  <script src="assets/lib/bootstrap/js/bootstrap.custom.min.js"></script>
  <script src="assets/js/app.js"></script>
  <script src="assets/js/yahoo.js"></script>
</body>
</html>
