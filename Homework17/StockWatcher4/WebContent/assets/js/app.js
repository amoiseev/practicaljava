// Namespace for the application
var SW = {};


/**
 * ==========================================================
 * Stock view model. Represents particular stock in the grid.
 * ==========================================================
 */

SW.Stock = function (rawData) {
    'use strict';

    // Initialize data with new empty object
    // if the provided value is null or undefined.
    rawData = rawData || {};

    this.symbol = ko.observable(rawData.symbol || '');
    this.name = ko.observable(rawData.name || 'N/A');
    this.price = ko.observable(rawData.price || 'N/A');
    this.change = ko.observable(rawData.change || '');
    this.marketCap = ko.observable(rawData.marketCap || 'N/A');
    this.isTemporary = ko.observable(true);
};

SW.Stock.prototype = {
    constructor: SW.Stock,

    set: function (rawData) {
        'use strict';

        rawData.symbol && this.symbol(rawData.symbol);
        rawData.name && this.name(rawData.name);
        rawData.price && this.price(rawData.price);
        rawData.change && this.change(rawData.change);
        rawData.marketCap && this.marketCap(rawData.marketCap);
    }
};


/**
 * ===============================================================
 * Portfolio view model. Represents the context for the main page.
 * ===============================================================
 */

SW.Portfolio = function () {
    'use strict';

    var quotesSocketUrl = "ws://" + window.location.hostname + ":" + window.location.port + "/StockWatcher4/quotesws",
        that = this;

    this.symbol = ko.observable('');
    this.stocks = ko.observableArray();
    this.invalidSymbol = ko.observable('');

    // Enable Live Update if WebSocket feature is available.
    if (window.WebSocket) {
        jQuery('#alert-websockets-enabled').show();

        this.quotesSocket = new WebSocket(quotesSocketUrl);
        this.quotesSocket.onmessage = function (e) {
            that.updateStock(jQuery.parseJSON(e.data));
        };
    } else {
        jQuery('#alert-websockets-disabled').show();
    }

    setInterval(this.updateQuotes, 5000);
};

SW.Portfolio.prototype = {
    constructor: SW.Portfolio,

    addStock: function (rawData) {
        'use strict';

        // Make symbol upper case to properly compare it
        // with existing stocks in the portfolio.
        var symbol = this.symbol().toUpperCase(),
            that = this,
            stock;

        // Don't fetch stock data if symbol is empty.
        if (symbol === '') {
            return;
        }

        // Try to find stock with specified symbol in the portfolio.
        stock = ko.utils.arrayFirst(this.stocks(), function(item) {
            return item.symbol() === symbol;
        });

        // If portfolio doesn't contain specified stock yet, try to add it.
        if (!stock) {
            stock = new SW.Stock({ symbol: symbol });
            this.stocks.push(stock);
            this.symbol('');

            // Make AJAX request to the server to get stock data.
            // When the response arrive, update existing entry
            // in the grid with real actual data.
            jQuery.getJSON('quotes', { s: symbol }, function (data) {
                if (data.symbol) {
                    stock.isTemporary(false);
                    stock.set(data);
                } else {
                    that.stocks.remove(stock);
                    that.invalidSymbol(symbol);
                    that.showAlert('alert-invalid-symbol');
                }
            });
        } else {
            this.symbol(symbol);
            this.showAlert('alert-duplicate-symbol');
        }
    },

    updateStock: function (rawData) {
       'use strict';

        var stock = ko.utils.arrayFirst(this.stocks(), function (s) {
            return s.symbol() === rawData.symbol;
        });

        stock.price(rawData.price);
        stock.change(rawData.change);
    },

    updateQuotes: function () {
        'use strict';

        var vm = SW.ViewModel,
            symbols = [];

        if (vm.stocks().length > 0) {
            jQuery.map(vm.stocks(), function (s) {
                if (!s.isTemporary()) {
                    symbols.push(s.symbol());
                }
            });

            vm.quotesSocket.send(symbols.join(','));
        }
    },

    showAlert: function (id) {
        'use strict';

        var alert = jQuery('#' + id);

        alert.fadeIn();
        setTimeout(function () {
            alert.fadeOut();
        }, 3000);
    }
};


/**
 * =====================================
 * Bind the main view model to the page.
 * =====================================
 */

SW.ViewModel = new SW.Portfolio();
ko.applyBindings(SW.ViewModel);
