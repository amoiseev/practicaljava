SYMBOL_TYPEAHEAD = {};

YAHOO = {};
YAHOO.Finance = {};
YAHOO.Finance.SymbolSuggest = {};
YAHOO.Finance.SymbolSuggest.ssCallback = function (data) {
    var stocks = jQuery.map(data.ResultSet.Result, function (s) {
        s.display = s.symbol + ' - ' + s.name;
        return s;
    });
    SYMBOL_TYPEAHEAD.process(stocks);
};

// Initialize symbols auto complete
jQuery('input#symbol').typeahead({
    property: 'display',
    source: function (typeahead, query) {
        SYMBOL_TYPEAHEAD = typeahead;
        jQuery.ajax({
            url: 'http://autoc.finance.yahoo.com/autoc',
            data: { query: query },
            dataType: 'jsonp',
            jsonpCallback: 'YAHOO.Finance.SymbolSuggest.ssCallback'
        });
    },
    onselect: function (item) {
        SW.ViewModel.symbol(item.symbol);
    }
});
