package com.practicaljava.stockwatcher.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.ejb.EJB;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;

@WebServlet(urlPatterns = "/quotes", asyncSupported = true)
public class StockQuotesServlet extends HttpServlet {
    private static final String MIME_JSON = "application/json";
    private static final String DISPATCH_MODE_PARAM = "mode";
    private static final String SYMBOL_PARAM = "s";

    @EJB
    private FinanceService financeService;

    /**
     * Retrieves requested symbol from the request object and fetches its
     * stock quotes using external finance service.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Find out how to dispatch request: synchronously or asynchronously
        String dispatchMode = request.getParameter(DISPATCH_MODE_PARAM);

        // Find out for which symbol stock quote is requested.
        String symbol = request.getParameter(SYMBOL_PARAM);

        // If a symbol is provided try to fetch stock quote from external service.
        if (symbol != null) {
            // Prepare request and response objects to pass to asynchronous handler.
            request.setAttribute("symbol", symbol);
            response.setContentType(MIME_JSON);

            if ("sync".equalsIgnoreCase(dispatchMode)) {
                // Synchronously dispatch request.
                StockQuote quote = financeService.fetchQuote(symbol);

                Gson gson = new Gson();
                String jsonQuote = gson.toJson(quote);

                PrintWriter out = response.getWriter();
                out.write(jsonQuote);
            } else {
                // Asynchronously dispatch request.
                AsyncContext asyncContext = request.startAsync(request, response);
                Executor executor = Executors.newCachedThreadPool();
                executor.execute(new StockQuotesAsyncProcessor(asyncContext, financeService));
            }
        }
    }

}
