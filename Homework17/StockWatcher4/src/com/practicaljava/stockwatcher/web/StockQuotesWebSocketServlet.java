package com.practicaljava.stockwatcher.web;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.model.FinanceService;
import com.practicaljava.stockwatcher.model.StockQuote;

@WebServlet(urlPatterns = "/quotesws")
public class StockQuotesWebSocketServlet extends WebSocketServlet {

    @EJB
    private FinanceService financeService;

    @Override
    protected StreamInbound createWebSocketInbound(String subProtocol) {

        return new MessageInbound() {
            @Override
            protected void onTextMessage(CharBuffer message) throws IOException {
                Gson gson = new Gson();

                // Parse incoming message to get stock symbols for update.
                String[] symbols = StringUtils.split(message.toString(), ',');

                // TODO: replace synchronous requests dispatching with asynchronous one
                for (String symbol: symbols) {
                    StockQuote stockQuote = financeService.fetchQuote(symbol);

                    CharBuffer response = CharBuffer.wrap(gson.toJson(stockQuote));
                    this.getWsOutbound().writeTextMessage(response);
                }
            }

            @Override
            protected void onBinaryMessage(ByteBuffer byteBuffer) throws IOException {
            }
        };
    }

}
