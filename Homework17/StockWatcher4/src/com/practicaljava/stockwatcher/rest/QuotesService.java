package com.practicaljava.stockwatcher.rest;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.practicaljava.stockwatcher.model.StockQuote;

@Path("/quotes")
@Produces("application/json")
public class QuotesService {

    @Path("{symbol}")
    public String get(@PathParam("symbol") String symbol) {
        StockQuote s = new StockQuote();
        s.symbol = symbol;
        return new Gson().toJson(s);
    }
}
