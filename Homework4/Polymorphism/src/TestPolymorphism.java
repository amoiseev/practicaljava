import com.practicaljava.homework4.Printable;
import com.practicaljava.homework4.WebPage;
import com.practicaljava.homework4.XpsDocument;

public class TestPolymorphism {

    public static void main(String[] args) {
        Printable[] documents = {
            new WebPage(),
            new XpsDocument()
        };

        for (Printable doc: documents) {
            doc.print();
        }
    }

}
