package com.practicaljava.homework4;

public class XpsDocument implements Printable {

    public void print() {
        for (int i = 1; i <= MAX_PAGES; i++) {
            System.out.printf("Printing XPS document; page #%d\n", i);
        }
    }

}
