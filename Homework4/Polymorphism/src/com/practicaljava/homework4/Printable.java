package com.practicaljava.homework4;

public interface Printable {
    final int MAX_PAGES = 2;
    void print();
}
