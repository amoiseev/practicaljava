package com.practicaljava.homework4;

public class WebPage implements Printable {

    public void print() {
        for (int i = 1; i <= MAX_PAGES; i++) {
            System.out.printf("Printing web page; page #%d\n", i);
        }
    }

}
