public class NameAndAddress {

    public static void main(String[] args) {
        String name = "Anton Moiseev";
        String addressLine1 = "350 5th Avenue";
        String addressLine2 = "New York, NY 10118";
        String country = "USA";

        System.out.printf("%s\n%s\n%s\n%s",
                name, addressLine1, addressLine2, country);
    }

}
