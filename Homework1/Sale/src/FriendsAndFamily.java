import java.util.Scanner;

public class FriendsAndFamily {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Infinitely asks to enter the price and discount,
        // then evaluates final price based on the entered values.
        do {
            System.out.println("Enter list price:");
            double listPrice = input.nextDouble();

            System.out.println("Enter discount %:");
            int discount = input.nextInt();

            double finalPrice = listPrice - listPrice * discount / 100; // 100 - percents 
            System.out.printf("You'll pay only $%2.2f\n", finalPrice);

        } while (true);
    }

}
